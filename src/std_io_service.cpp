#include <iostream>
#include "std_io_service.hpp"

using std::cout;
using std::endl;
using std::cin;

void StdIoService::write(const string message) const {
    cout << message << endl;
}

string StdIoService::read(const string prompt) const {
    cout << prompt;
    string line;
    getline(cin, line);
    return line;
}
