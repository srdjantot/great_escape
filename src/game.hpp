#ifndef GREAT_ESCAPE_GAME_HPP
#define GREAT_ESCAPE_GAME_HPP


#include "data/game_state.hpp"
#include "io_service.hpp"
#include "command/parser.hpp"

class Game {
public:
    Game(IoService &ioService, GameState &gameState);
    bool isRunning() const;
    void parseInput();
private:
    const IoService &ioService;
    const Parser parser;
    GameState &gameState;
};


#endif //GREAT_ESCAPE_GAME_HPP
