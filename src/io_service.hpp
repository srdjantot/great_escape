#ifndef GREAT_ESCAPE_IO_SERVICE_HPP
#define GREAT_ESCAPE_IO_SERVICE_HPP


#include <string>

using std::string;

class IoService {
public:
    virtual void write(const string message) const = 0;
    virtual string read(const string prompt) const = 0;
};


#endif //GREAT_ESCAPE_IO_SERVICE_HPP
