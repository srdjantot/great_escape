#ifndef GREAT_ESCAPE_GAME_SERIALIZER_HPP
#define GREAT_ESCAPE_GAME_SERIALIZER_HPP


#include <iostream>
#include "game_state.hpp"

using std::istream;
using std::ostream;

class GameSerializer {
public:
    void load(istream &stream, GameState &gameState) const;
    void save(ostream &stream, const GameState &gameState) const;
};

#endif //GREAT_ESCAPE_GAME_SERIALIZER_HPP
