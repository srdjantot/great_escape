#ifndef GREAT_ESCAPE_STAGE_HPP
#define GREAT_ESCAPE_STAGE_HPP


#include <unordered_map>
#include <memory>
#include "entity.hpp"
#include "item.hpp"

using std::unordered_map;
using std::weak_ptr;
using std::shared_ptr;

struct Stage : Entity {
    unordered_map<string, weak_ptr<Stage>> portals;
    vector<shared_ptr<Item>> items;
};


#endif //GREAT_ESCAPE_STAGE_HPP
