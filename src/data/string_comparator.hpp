#ifndef GREAT_ESCAPE_STRING_COMPARATOR_HPP
#define GREAT_ESCAPE_STRING_COMPARATOR_HPP


#include <string>
#include <vector>

using std::string;
using std::equal;
using std::vector;

bool icompare_pred(unsigned char a, unsigned char b)
{
    return tolower(a) == tolower(b);
}

bool icompare(const string &a, const string &b)
{
    return (a.length() == b.length()) ? equal(b.begin(), b.end(), a.begin(), icompare_pred) : false;
}

bool icompare(const vector<string> &v, const string &b) {
    for (auto &a : v)
        if (icompare(a, b))
            return true;
    return false;
}

#endif //GREAT_ESCAPE_STRING_COMPARATOR_HPP
