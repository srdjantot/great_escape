#ifndef GREAT_ESCAPE_GAME_STATE_HPP
#define GREAT_ESCAPE_GAME_STATE_HPP


#include "command/command.hpp"
#include "stage.hpp"
#include "item.hpp"

using std::shared_ptr;

struct GameState {
    bool running;
    string welcomeMessage;
    string invalidCommand;
    vector<Command> commands;
    vector<shared_ptr<Stage>> stages;
    shared_ptr<Stage> currentStage;
    vector<shared_ptr<Item>> items;
    vector<shared_ptr<Item>> inventory;
};


#endif //GREAT_ESCAPE_GAME_STATE_HPP
