#ifndef GREAT_ESCAPE_ENTITY_HPP
#define GREAT_ESCAPE_ENTITY_HPP


#include <string>
#include <vector>

using std::string;
using std::vector;

struct Entity {
    string id;
    vector<string> names;
    string description;
};


#endif //GREAT_ESCAPE_ENTITY_HPP
