#include "game_serializer.hpp"
#include "cereal/archives/portable_binary.hpp"
#include "cereal/types/unordered_map.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/memory.hpp"

using cereal::PortableBinaryInputArchive;
using cereal::PortableBinaryOutputArchive;
using cereal::base_class;

template<class Archive>
void serialize(Archive &archive, Command &o)
{
    archive(o.pattern, o.usage, o.error, o.transforms);
}

template<class Archive>
void serialize(Archive &archive, Entity &o)
{
    archive(o.id, o.names, o.description);
}

template<class Archive>
void serialize(Archive &archive, Stage &o)
{
    archive(base_class<Entity>(&o), o.portals, o.items);
}

template<class Archive>
void serialize(Archive &archive, Item &o)
{
    archive(base_class<Entity>(&o), o.requirements, o.takeActions, o.useActions);
}

template<class Archive>
void serialize(Archive &archive, GameState &o)
{
    archive(o.welcomeMessage, o.invalidCommand, o.commands, o.stages, o.currentStage, o.items, o.inventory);
}

void GameSerializer::load(istream &stream, GameState &gameState) const {
    PortableBinaryInputArchive archive(stream);
    archive(gameState);
}

void GameSerializer::save(ostream &stream, const GameState &gameState) const {
    PortableBinaryOutputArchive archive(stream);
    archive(gameState);
}
