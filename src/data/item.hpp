#ifndef GREAT_ESCAPE_ITEM_HPP
#define GREAT_ESCAPE_ITEM_HPP


#include "entity.hpp"

using std::weak_ptr;

struct Item : Entity {
    vector<weak_ptr<Item>> requirements;
    vector<string> takeActions;
    vector<string> useActions;
};


#endif //GREAT_ESCAPE_ITEM_HPP
