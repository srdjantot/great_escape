#include <exception>
#include "game.hpp"

using std::invalid_argument;

Game::Game(IoService &ioService, GameState &gameState) : ioService(ioService), parser(ioService, gameState), gameState(gameState) {
    gameState.running = true;
    parser.execute("write " + gameState.welcomeMessage);
    parser.execute("write_stage _.description");
}

bool Game::isRunning() const {
    return gameState.running;
}

void Game::parseInput() {
    string line = ioService.read(" > ");
    try {
        string error;
        auto commands = parser.parse(line, error);
        try {
            for (auto &o : commands)
                parser.execute(o);
        } catch (invalid_argument) {
            ioService.write(error);
        }
    } catch (invalid_argument) {
        ioService.write(gameState.invalidCommand);
    }
}
