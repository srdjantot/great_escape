#ifndef GREAT_ESCAPE_PARSER_HPP
#define GREAT_ESCAPE_PARSER_HPP


#include <functional>
#include "io_service.hpp"
#include "data/game_state.hpp"
#include "command.hpp"
#include "executor.hpp"

using std::function;

class Parser {
public:
    Parser(IoService &ioService, GameState &gameState);
    vector<string> parse(const string line, string &error) const;
    void execute(const string command) const;
private:
    shared_ptr<Stage> findStageById(const string id) const;
    void writeStageMember(const shared_ptr<Stage> stage, const string member) const;
    shared_ptr<Item> findItemById(const string id, const bool all) const;
    shared_ptr<Item> findItemByName(const string name, const bool all) const;
    void writeItemMember(const shared_ptr<Item> item, const string member) const;
    const IoService &ioService;
    GameState &gameState;
    vector<Executor> executors;
};


#endif //GREAT_ESCAPE_PARSER_HPP
