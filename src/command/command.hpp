#ifndef GREAT_ESCAPE_COMMAND_HPP
#define GREAT_ESCAPE_COMMAND_HPP


#include <string>
#include <vector>

using std::string;
using std::vector;

struct Command {
    string pattern;
    string usage;
    string error;
    vector<string> transforms;
};


#endif //GREAT_ESCAPE_COMMAND_HPP
