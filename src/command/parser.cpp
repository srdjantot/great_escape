#include <regex>
#include "parser.hpp"
#include "data/string_comparator.hpp"

using std::regex;
using std::regex_match;
using std::regex_replace;
using std::sregex_iterator;
using std::regex_constants::extended;
using std::regex_constants::icase;
using std::regex_constants::format_no_copy;
using std::invalid_argument;
using std::remove;
using std::find;

template<typename T>
void removeFromVector(vector<T> &v, T &i) {
    v.erase(remove(v.begin(), v.end(), i));
}

Parser::Parser(IoService &ioService, GameState &gameState) : ioService(ioService), gameState(gameState) {
    executors = {
            {"^help$", [&](vector<string>) {
                for(auto &command : gameState.commands) {
                    ioService.write(command.usage);
                }
            }},
            {"^exit$", [&](vector<string>) {
                gameState.running = false;
            }},
            {"^go (.*)$", [&](vector<string> args) {
                auto stage = gameState.currentStage->portals.find(args[0]);
                if (stage == gameState.currentStage->portals.end())
                    throw invalid_argument("direction");
                gameState.currentStage = stage->second.lock();
            }},
            {"^write_stage (.*)\\.(.*)$", [&](vector<string> args) {
                writeStageMember(findStageById(args[0]), args[1]);
            }},
            {"^change_stage_description (.*) => (.*)$", [&](vector<string> args) {
                findStageById(args[0])->description = args[1];
            }},
            {"^add_stage_item (.*) => (.*)$", [&](vector<string> args) {
                auto stage = findStageById(args[0]);
                auto item = findItemById(args[1], true);

                if (find(stage->items.begin(), stage->items.end(), item) != stage->items.end())
                    throw invalid_argument("stage " + args[0] + "already have item " + args[1]);

                stage->items.push_back(item);
            }},
            {"^remove_stage_item (.*) => (.*)$", [&](vector<string> args) {
                auto stage = findStageById(args[0]);
                auto item = findItemById(args[1], true);

                if (find(stage->items.begin(), stage->items.end(), item) == stage->items.end())
                    throw invalid_argument("stage " + args[0] + "doesn't have item " + args[1]);

                removeFromVector(stage->items, item);
            }},
            {"^add_stage_portal (.*) => (.*)\\.(.*)$", [&](vector<string> args) {
                auto stage = findStageById(args[0]);
                auto portal = findStageById(args[2]);

                if (stage->portals.find(args[1]) != stage->portals.end())
                    throw invalid_argument("stage " + args[0] + "already have portal " + args[1]);

                stage->portals.insert({args[1], portal});
            }},
            {"^remove_stage_portal (.*) => (.*)$", [&](vector<string> args) {
                auto stage = findStageById(args[0]);
                if (stage->portals.find(args[1]) == stage->portals.end())
                    throw invalid_argument("stage " + args[0] + "doesn't have portal " + args[1]);
                stage->portals.erase(args[1]);
            }},
            {"^write_item (.*)\\.(.*)$", [&](vector<string> args) {
                auto item = findItemByName(args[0], false);
                writeItemMember(item, args[1]);
            }},
            {"^change_item_description (.*) => (.*)$", [&](vector<string> args) {
                findItemById(args[0], true)->description = args[1];
            }},
            {"^take_item (.*)$", [&](vector<string> args) {
                auto item = findItemByName(args[0], false);

                if (item->id[0] == '_')
                    throw invalid_argument("private item " + args[0]);

                if (find(gameState.inventory.begin(), gameState.inventory.end(), item) != gameState.inventory.end())
                    throw invalid_argument("already taken " + args[0]);

                removeFromVector(gameState.currentStage->items, item);
                gameState.inventory.push_back(item);
                for(auto &action : item->takeActions)
                    execute(action);
            }},
            {"^use_item (.*)$", [&](vector<string> args) {
                auto item = findItemByName(args[0], false);

                if (item->id[0] == '_')
                    throw invalid_argument("private item " + args[0]);

                if (find(gameState.inventory.begin(), gameState.inventory.end(), item) == gameState.inventory.end())
                    throw invalid_argument("must take " + args[0]);

                for(auto &i : item->requirements)
                    findItemById(i.lock()->id, false);

                for(auto &action : item->useActions)
                    execute(action);
            }},
            {"^inventory$", [&](vector<string> args) {
                for (auto &item : gameState.inventory)
                    ioService.write(item->names[0]);
                ioService.write("");
            }},
            {"^write (.*)$", [&](vector<string> args) {
                ioService.write(args[0]);
            }}
    };
}

vector<string> Parser::parse(const string line, string &error) const {
    if (line.empty())
        return vector<string>(0);

    for(auto &command : gameState.commands) {
        regex pattern(command.pattern, extended | icase);
        if (regex_match(line, pattern)) {
            error = regex_replace(line, pattern, command.error, format_no_copy);

            vector<string> result(command.transforms);
            for(auto &transformed : result) {
                transformed = regex_replace(line, pattern, transformed, format_no_copy);
            }
            return result;
        }
    }

    throw invalid_argument("line");
}

void Parser::execute(const string command) const {
    for(auto &executor : executors) {
        regex pattern(executor.pattern, extended);
        if (regex_match(command, pattern)) {
            auto match = sregex_iterator(command.begin(), command.end(), pattern);
            vector<string> args;
            for(auto arg = ++match->begin(); arg != match->end(); ++arg)
                args.push_back(*arg);
            executor.execute(args);
            return;
        }
    }
}

shared_ptr<Stage> Parser::findStageById(const string id) const {
    if (id == "_")
        return gameState.currentStage;

    for(auto &stage : gameState.stages)
        if (stage->id == id)
            return stage;

    throw invalid_argument("id");
}

void Parser::writeStageMember(const shared_ptr<Stage> stage, const string member) const {
    if (member == "name")
        ioService.write(stage->names[0]);
    else if (member == "description")
        ioService.write(stage->description);
    else if (member == "portals") {
        for(auto &portal : stage->portals)
            ioService.write(portal.first + " -> " + portal.second.lock()->names[0]);
    }
    else
        throw invalid_argument("stage." + member);
}

shared_ptr<Item> Parser::findItemById(const string id, const bool all) const {
    if (all) {
        for (auto &item : gameState.items)
            if (item->id == id)
                return item;
    } else {
        for (auto &item : gameState.inventory)
            if (item->id == id)
                return item;

        for (auto &item : gameState.currentStage->items)
            if (item->id == id)
                return item;
    }
    throw invalid_argument("id");
}

shared_ptr<Item> Parser::findItemByName(const string name, const bool all) const {
    if (all) {
        for (auto &item : gameState.items)
            if (icompare(item->names, name))
                return item;
    } else {
        for (auto &item : gameState.inventory)
            if (icompare(item->names, name))
                return item;

        for (auto &item : gameState.currentStage->items)
            if (icompare(item->names, name))
                return item;
    }

    throw invalid_argument("name");
}

void Parser::writeItemMember(const shared_ptr<Item> item, const string member) const {
    if (member == "name")
        ioService.write(item->names[0]);
    else if (member == "description")
        ioService.write(item->description);
    else
        throw invalid_argument("stage." + member);
}
