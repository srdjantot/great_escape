#ifndef GREAT_ESCAPE_EXECUTOR_HPP
#define GREAT_ESCAPE_EXECUTOR_HPP


#include <string>
#include <functional>
#include <vector>

using std::string;
using std::function;
using std::vector;

struct Executor {
    string pattern;
    function<void(vector<string>)> execute;
};


#endif //GREAT_ESCAPE_EXECUTOR_HPP
