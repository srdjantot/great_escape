#include <fstream>
#include "data/game_serializer.hpp"
#include "std_io_service.hpp"
#include "game.hpp"

using std::make_shared;

int main() {
    StdIoService ioService;
    GameState gameState;
    GameSerializer serializer;
    std::ifstream is("game.bin", std::ios::binary);
    serializer.load(is, gameState);

    Game game(ioService, gameState);
    while(game.isRunning())
        game.parseInput();

    return 0;
}