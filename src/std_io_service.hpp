#ifndef GREAT_ESCAPE_STD_IO_SERVICE_HPP
#define GREAT_ESCAPE_STD_IO_SERVICE_HPP


#include "io_service.hpp"

class StdIoService : public IoService {
public:
    virtual void write(const string message) const override;
    virtual string read(const string prompt) const override;
};


#endif //GREAT_ESCAPE_STD_IO_SERVICE_HPP
