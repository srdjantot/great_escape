#include "catch/catch.hpp"
#include "fakeit/fakeit.hpp"
#include "game.hpp"

using std::make_shared;

using fakeit::Mock;
using fakeit::When;
using fakeit::Verify;
using fakeit::Fake;

SCENARIO("game initialization", "[game]") {

    GIVEN("simple game state") {
        auto stage = make_shared<Stage>();
        stage->id = "stage";
        stage->names = {"Stage"};
        stage->description = "You are standing in stage.\n";

        GameState gameState;
        gameState.welcomeMessage = "Welcome!\n";
        gameState.commands = {
                {"^exit|quit$", "", "", {"exit"}}
        };
        gameState.stages.push_back(stage);
        gameState.currentStage = stage;

        Mock<IoService> ioService;
        Fake(Method(ioService, write));

        WHEN("the game is constructed") {
            Game game(ioService.get(), gameState);

            THEN("the game should be running") {
                REQUIRE(game.isRunning());
            }

            THEN("the game should write welcome message") {
                Verify(Method(ioService, write).Using("Welcome!\n"));
            }

            THEN("the game should write current stage description") {
                Verify(Method(ioService, write).Using("You are standing in stage.\n"));
            }

            THEN("the game should run until exit command") {
                When(Method(ioService, read)).Return("", "exit");
                REQUIRE(game.isRunning());
                game.parseInput();
                REQUIRE(game.isRunning());
                game.parseInput();
                REQUIRE(!game.isRunning());
            }
        }
    }
}