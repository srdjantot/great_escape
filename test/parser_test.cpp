#include "catch/catch.hpp"
#include "fakeit/fakeit.hpp"
#include "command/parser.hpp"

using std::make_shared;
using std::invalid_argument;

using fakeit::Mock;
using fakeit::When;
using fakeit::Verify;
using fakeit::Fake;

SCENARIO("parsing commands", "[parser]") {

    GIVEN("simple game state with two stages") {
        auto stage1 = make_shared<Stage>();
        stage1->id = "stage1";
        stage1->names = {"Stage1"};
        stage1->description = "You are standing in stage 1.\n";

        auto stage2 = make_shared<Stage>();
        stage2->id = "stage2";
        stage2->names = {"Stage2"};
        stage2->description = "You are standing in stage 2.\n";

        stage1->portals.insert({"north", stage2});
        stage2->portals.insert({"south", stage1});

        auto item1 = make_shared<Item>();
        item1->id = "item1";
        item1->names = {"Item1", "Item 1"};
        item1->description = "You are looking at item 1.\n";
        item1->takeActions = {"write You took item 1.\n"};
        item1->useActions = {"write You used item 1.\n"};

        auto item2 = make_shared<Item>();
        item2->id = "_item2";
        item2->names = {"Item2"};
        item1->requirements.push_back(item2);

        stage1->items.push_back(item1);
        stage2->items.push_back(item2);

        GameState gameState;
        gameState.welcomeMessage = "Welcome!\n";
        gameState.invalidCommand = "Uh, oh... invalid command...\nType help for list of available commands.\n";
        gameState.commands = {
                {"^help$", "help", "", {"help"}},
                {"^exit$", "exit", "", {"exit"}},
                {"^go (.*)$", "go", "You can't go $1.", {"go $1"}},
                {"^multiple$", "multiple", "", {"write one", "write two"}},
                {"^multiple params (.) (.)$", "multiple params", "", {"write $1", "write $2"}},
                {"^test$", "test", "",
                        {
                                "write_stage _.name",
                                "write_stage _.description",
                                "write_stage _.portals",
                                "write_stage stage2.name",
                                "write_stage stage2.description",
                                "write_stage stage2.portals",
                                "write_stage _.invalid",
                                "write_stage invalid.name",

                                "change_stage_description _ => test",
                                "change_stage_description invalid => test",

                                "add_stage_item _ => _item2",
                                "add_stage_item _ => _item2",
                                "add_stage_item invalid => _item2",
                                "add_stage_item _ => invalid",

                                "remove_stage_item _ => _item2",
                                "remove_stage_item _ => _item2",
                                "remove_stage_item invalid => _item2",
                                "remove_stage_item _ => invalid",

                                "write_item item1.name",
                                "write_item item1.description",
                                "write_item invalid.name",
                                "write_item item1.invalid",

                                "change_item_description item1 => test",
                                "change_item_description invalid => test",

                                "use_item item1",

                                "take_item item1",
                                "take_item item1",
                                "take_item invalid",
                                "add_stage_item _ => _item2",
                                "take_item item2",

                                "use_item item1",
                                "use_item item2",

                                "write Test test test",

                                "add_stage_portal _ => up._",
                                "add_stage_portal _ => up._",
                                "add_stage_portal invalid => up._",
                                "add_stage_portal _ => down.invalid",

                                "remove_stage_portal _ => up",
                                "remove_stage_item _ => up",
                                "remove_stage_item invalid => up",
                                "remove_stage_item _ => down",

                                "write_item item 1.name",
                                "inventory"
                        }},
        };
        gameState.stages.push_back(stage1);
        gameState.stages.push_back(stage2);
        gameState.currentStage = stage1;

        gameState.items.push_back(item1);
        gameState.items.push_back(item2);
        gameState.running = true;

        Mock<IoService> ioService;
        Fake(Method(ioService, write));

        Parser parser(ioService.get(), gameState);
        string error;

        WHEN("user type invalid command") {
            THEN("parser should raise invalid_argument exception") {
                REQUIRE_THROWS_AS(parser.parse("invalid", error), invalid_argument);
            }
        }

        WHEN("user type help") {
            auto exec = parser.parse("help", error);

            THEN("parser should return help executor") {
                REQUIRE(exec.size() == 1);
                REQUIRE(exec[0] == "help");

                WHEN("parser executes help executor") {
                    for(auto &e : exec)
                        parser.execute(e);

                    THEN("parser should display list of available commands") {
                        Verify(Method(ioService, write).Using("help"));
                        Verify(Method(ioService, write).Using("exit"));
                        Verify(Method(ioService, write).Using("go"));
                        Verify(Method(ioService, write).Using("multiple"));
                        Verify(Method(ioService, write).Using("multiple params"));
                        Verify(Method(ioService, write).Using("test"));
                    }
                }
            }
        }

        WHEN("user type exit") {
            auto exec = parser.parse("exit", error);

            THEN("parser should return exit executor") {
                REQUIRE(exec.size() == 1);
                REQUIRE(exec[0] == "exit");

                WHEN("parser executes exit executor") {
                    for(auto &e : exec)
                        parser.execute(e);

                    THEN("game should not be running") {
                        REQUIRE(!gameState.running);
                    }
                }
            }
        }

        WHEN("user type go north") {
            auto exec = parser.parse("go north", error);

            THEN("parser should return go executor") {
                REQUIRE(exec.size() == 1);
                REQUIRE(exec[0] == "go north");

                WHEN("parser executes go executor") {
                    for(auto &e : exec)
                        parser.execute(e);

                    THEN("current stage should be stage2") {
                        REQUIRE(gameState.currentStage == stage2);
                    }
                }
            }
        }

        WHEN("user type go up") {
            parser.parse("go up", error);

            THEN("parser should return error") {
                REQUIRE(error == "You can't go up.");
            }
        }

        WHEN("user type multiple") {
            auto exec = parser.parse("multiple", error);

            THEN("parser should return two write executors") {
                REQUIRE(exec.size() == 2);
                REQUIRE(exec[0] == "write one");
                REQUIRE(exec[1] == "write two");

                WHEN("parser executes write executors") {
                    for(auto &e : exec)
                        parser.execute(e);

                    THEN("parser should display text \"one\" and \"two\"") {
                        Verify(Method(ioService, write).Using("one"));
                        Verify(Method(ioService, write).Using("two"));
                    }
                }
            }
        }

        WHEN("user type multiple params 1 2") {
            auto exec = parser.parse("multiple params 1 2", error);

            THEN("parser should return two write executors") {
                REQUIRE(exec.size() == 2);
                REQUIRE(exec[0] == "write 1");
                REQUIRE(exec[1] == "write 2");

                WHEN("parser executes write executors") {
                    for(auto &e : exec)
                        parser.execute(e);

                    THEN("parser should display text \"1\" and \"2\"") {
                        Verify(Method(ioService, write).Using("1"));
                        Verify(Method(ioService, write).Using("2"));
                    }
                }
            }
        }

        WHEN("user type test") {
            auto exec = parser.parse("test", error);

            THEN("parser should execute test commands") {
                REQUIRE(exec.size() == 43);
                parser.execute(exec[0]); // "write_stage _.name",
                Verify(Method(ioService, write).Using("Stage1"));

                parser.execute(exec[1]); // "write_stage _.description",
                Verify(Method(ioService, write).Using("You are standing in stage 1.\n"));

                parser.execute(exec[2]); // "write_stage _.portals",
                Verify(Method(ioService, write).Using("north -> Stage2"));

                parser.execute(exec[3]); // "write_stage stage2.name",
                Verify(Method(ioService, write).Using("Stage2"));

                parser.execute(exec[4]); // "write_stage stage2.description",
                Verify(Method(ioService, write).Using("You are standing in stage 2.\n"));

                parser.execute(exec[5]); // "write_stage stage2.portals",
                Verify(Method(ioService, write).Using("south -> Stage1"));

                REQUIRE_THROWS_AS(parser.execute(exec[6]), invalid_argument); // "write_stage _.invalid",
                REQUIRE_THROWS_AS(parser.execute(exec[7]), invalid_argument); // "write_stage invalid.name",

                parser.execute(exec[8]); // "change_stage_description _ => test",
                REQUIRE(stage1->description == "test");
                REQUIRE_THROWS_AS(parser.execute(exec[9]),
                                  invalid_argument); // "change_stage_description invalid => test",

                parser.execute(exec[10]); // "add_stage_item _ => _item2",
                REQUIRE(stage1->items[1] == item2);
                REQUIRE_THROWS_AS(parser.execute(exec[11]), invalid_argument); // "add_stage_item _ => _item2",
                REQUIRE_THROWS_AS(parser.execute(exec[12]), invalid_argument); // "add_stage_item invalid => _item2",
                REQUIRE_THROWS_AS(parser.execute(exec[13]), invalid_argument); // "add_stage_item _ => invalid",

                parser.execute(exec[14]); // "remove_stage_item _ => _item2",
                REQUIRE(stage1->items.size() == 1);
                REQUIRE_THROWS_AS(parser.execute(exec[15]), invalid_argument); // "remove_stage_item _ => _item2",
                REQUIRE_THROWS_AS(parser.execute(exec[16]), invalid_argument); // "remove_stage_item invalid => _item2",
                REQUIRE_THROWS_AS(parser.execute(exec[17]), invalid_argument); // "remove_stage_item _ => invalid",

                parser.execute(exec[18]); // "write_item item1.name",
                Verify(Method(ioService, write).Using("Item1"));

                parser.execute(exec[19]); // "write_item item1.description",
                Verify(Method(ioService, write).Using("You are looking at item 1.\n"));

                REQUIRE_THROWS_AS(parser.execute(exec[20]), invalid_argument); // "write_item invalid.name",
                REQUIRE_THROWS_AS(parser.execute(exec[21]), invalid_argument); // "write_item item1.invalid",

                parser.execute(exec[22]); // "change_item_description item1 => test",
                REQUIRE(item1->description == "test");
                REQUIRE_THROWS_AS(parser.execute(exec[23]),
                                  invalid_argument); // "change_item_description invalid => test",

                REQUIRE_THROWS_AS(parser.execute(exec[24]), invalid_argument); // "use_item item1",

                parser.execute(exec[25]); // "take_item item1",
                REQUIRE(stage1->items.size() == 0);
                REQUIRE(gameState.inventory.size() == 1);
                Verify(Method(ioService, write).Using("You took item 1.\n"));

                REQUIRE_THROWS_AS(parser.execute(exec[26]), invalid_argument); // "take_item item1",
                REQUIRE_THROWS_AS(parser.execute(exec[27]), invalid_argument); // "take_item invalid",
                parser.execute(exec[28]); // "add_stage_item _ => _item2",
                REQUIRE(stage1->items[0] == item2);

                REQUIRE_THROWS_AS(parser.execute(exec[29]), invalid_argument); // "take_item item2",

                parser.execute(exec[30]); // "use_item item1",
                Verify(Method(ioService, write).Using("You used item 1.\n"));

                REQUIRE_THROWS_AS(parser.execute(exec[31]), invalid_argument); // "use_item item2",

                parser.execute(exec[32]); // "write Test test test",
                Verify(Method(ioService, write).Using("Test test test"));

                parser.execute(exec[33]); // "add_stage_portal _ => up._",
                REQUIRE(stage1->portals.find(string("up")) != stage1->portals.end());

                REQUIRE_THROWS_AS(parser.execute(exec[34]), invalid_argument); // "add_stage_portal _ => up._",
                REQUIRE_THROWS_AS(parser.execute(exec[35]), invalid_argument); // "add_stage_portal invalid => up._",
                REQUIRE_THROWS_AS(parser.execute(exec[36]), invalid_argument); // "add_stage_portal _ => down.invalid",

                parser.execute(exec[37]); // "remove_stage_portal _ => up",
                REQUIRE(stage1->portals.find(string("up")) == stage1->portals.end());
                REQUIRE_THROWS_AS(parser.execute(exec[38]), invalid_argument); // "remove_stage_item _ => up",
                REQUIRE_THROWS_AS(parser.execute(exec[39]), invalid_argument); // "remove_stage_item invalid => up",
                REQUIRE_THROWS_AS(parser.execute(exec[40]), invalid_argument); // "remove_stage_item _ => down",

                parser.execute(exec[41]); // "write_item item 1.name",
                Verify(Method(ioService, write).Using("Item1"));

                parser.execute(exec[42]); // "inventory",
                Verify(Method(ioService, write).Using("Item1"));
                Verify(Method(ioService, write).Using(""));
            }
        }
    }
}
